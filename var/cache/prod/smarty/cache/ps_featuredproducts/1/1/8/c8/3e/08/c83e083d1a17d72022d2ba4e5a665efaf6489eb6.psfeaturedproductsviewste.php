<?php
/* Smarty version 3.1.33, created on 2019-07-10 17:03:25
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d25fe3d8279f8_66360180',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1554227646,
      2 => 'module',
    ),
    '4c73a61b1c39d845436b133592fdd79d88b50660' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/catalog/_partials/miniatures/product-homepage.tpl',
      1 => 1554227646,
      2 => 'file',
    ),
    '205e0f1f76153e24eaf86744eb136799809fbf59' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/catalog/_partials/variant-links.tpl',
      1 => 1554227646,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5d25fe3d8279f8_66360180 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?><div class="nav-item"><a data-toggle="tab" class="nav-link" href="#featured_products">Featured Products</a></div> 
<section class="featured-products clearfix m-t-2 tab-pane" id="featured_products">
  <div class="products homepage-products-slider">
           
 

  <article class="product-miniature js-product-miniature" data-id-product="1" data-id-product-attribute="1" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/hommes/1-1-hummingbird-printed-t-shirt.html#/1-taille-s/8-couleur-blanc" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/2-home_default/hummingbird-printed-t-shirt.jpg"
            alt = "T-shirt imprimé colibri"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/2-large_default/hummingbird-printed-t-shirt.jpg"
          >
          
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price">28,68 €</span>
                                  <span class="discount-percentage">-20%</span>
                              
              

              <span itemprop="price" class="price" style="color: ">22,94 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="discount"><span>Prix réduit</span></li>
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/hommes/1-1-hummingbird-printed-t-shirt.html#/1-taille-s/8-couleur-blanc">T-shirt imprimé colibri</a></h1>
        

        
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price">28,68 €</span>
                                  <span class="discount-percentage">-20%</span>
                              
              

              <span itemprop="price" class="price" style="color: ">22,94 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/hommes/1-1-hummingbird-printed-t-shirt.html#/1-taille-s/8-couleur-blanc" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
                  <div class="variant-links">
      <a href="http://localhost:8888/prestaRunway/hommes/1-3-hummingbird-printed-t-shirt.html#/2-taille-m/8-couleur-blanc"
       class="color"
       title="Blanc"
              style="background-color: #ffffff"           ><span class="sr-only">Blanc</span></a>
      <a href="http://localhost:8888/prestaRunway/hommes/1-2-hummingbird-printed-t-shirt.html#/1-taille-s/11-couleur-noir"
       class="color"
       title="Noir"
              style="background-color: #434A54"           ><span class="sr-only">Noir</span></a>
    <span class="js-count count"></span>
</div>
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="2" data-id-product-attribute="9" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/femmes/2-9-brown-bear-printed-sweater.html#/1-taille-s" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/21-home_default/brown-bear-printed-sweater.jpg"
            alt = "Pull imprimé ours brun"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/21-large_default/brown-bear-printed-sweater.jpg"
          >
          
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price">43,08 €</span>
                                  <span class="discount-percentage">-20%</span>
                              
              

              <span itemprop="price" class="price" style="color: ">34,46 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="discount"><span>Prix réduit</span></li>
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/femmes/2-9-brown-bear-printed-sweater.html#/1-taille-s">Pull imprimé colibri</a></h1>
        

        
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price">43,08 €</span>
                                  <span class="discount-percentage">-20%</span>
                              
              

              <span itemprop="price" class="price" style="color: ">34,46 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/femmes/2-9-brown-bear-printed-sweater.html#/1-taille-s" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="3" data-id-product-attribute="13" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/art/3-13-affiche-encadree-the-best-is-yet-to-come.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/3-home_default/affiche-encadree-the-best-is-yet-to-come.jpg"
            alt = "Affiche encadrée The best is yet to come"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/3-large_default/affiche-encadree-the-best-is-yet-to-come.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">34,80 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/art/3-13-affiche-encadree-the-best-is-yet-to-come.html#/19-dimension-40x60cm">Affiche encadrée The best...</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">34,80 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/art/3-13-affiche-encadree-the-best-is-yet-to-come.html#/19-dimension-40x60cm" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="4" data-id-product-attribute="16" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/art/4-16-affiche-encadree-the-adventure-begins.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/4-home_default/affiche-encadree-the-adventure-begins.jpg"
            alt = "Affiche encadrée The adventure begins"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/4-large_default/affiche-encadree-the-adventure-begins.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">34,80 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/art/4-16-affiche-encadree-the-adventure-begins.html#/19-dimension-40x60cm">Affiche encadrée The...</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">34,80 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/art/4-16-affiche-encadree-the-adventure-begins.html#/19-dimension-40x60cm" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="5" data-id-product-attribute="19" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/art/5-19-affiche-encadree-today-is-a-good-day.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/5-home_default/affiche-encadree-today-is-a-good-day.jpg"
            alt = "Affiche encadrée Today is a good day"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/5-large_default/affiche-encadree-today-is-a-good-day.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">34,80 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/art/5-19-affiche-encadree-today-is-a-good-day.html#/19-dimension-40x60cm">Affiche encadrée Today is a...</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">34,80 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/art/5-19-affiche-encadree-today-is-a-good-day.html#/19-dimension-40x60cm" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="6" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/accessoires-de-maison/6-mug-the-best-is-yet-to-come.html" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/6-home_default/mug-the-best-is-yet-to-come.jpg"
            alt = "Mug The best is yet to come"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/6-large_default/mug-the-best-is-yet-to-come.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">14,28 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/accessoires-de-maison/6-mug-the-best-is-yet-to-come.html">Mug The best is yet to come</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">14,28 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/accessoires-de-maison/6-mug-the-best-is-yet-to-come.html" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="7" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/accessoires-de-maison/7-mug-the-adventure-begins.html" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/7-home_default/mug-the-adventure-begins.jpg"
            alt = "Mug The adventure begins"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/7-large_default/mug-the-adventure-begins.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">14,28 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/accessoires-de-maison/7-mug-the-adventure-begins.html">Mug The adventure begins</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">14,28 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/accessoires-de-maison/7-mug-the-adventure-begins.html" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="8" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/accessoires-de-maison/8-mug-today-is-a-good-day.html" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/8-home_default/mug-today-is-a-good-day.jpg"
            alt = "Mug Today is a good day"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/8-large_default/mug-today-is-a-good-day.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">14,28 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/accessoires-de-maison/8-mug-today-is-a-good-day.html">Mug Today is a good day</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">14,28 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/accessoires-de-maison/8-mug-today-is-a-good-day.html" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


      </div>
</section>
<?php }
}
