<?php
/* Smarty version 3.1.33, created on 2019-07-10 17:03:26
  from 'module:psspecialsviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d25fe3ed0eda7_54052152',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '69eca6f7099f96303240f391e6c6743858b25719' => 
    array (
      0 => 'module:psspecialsviewstemplatesh',
      1 => 1554227646,
      2 => 'module',
    ),
    '4c73a61b1c39d845436b133592fdd79d88b50660' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/catalog/_partials/miniatures/product-homepage.tpl',
      1 => 1554227646,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5d25fe3ed0eda7_54052152 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>
<div class="nav-item"><a data-toggle="tab" class="nav-link" href="#specials">On Sale</a></div>
<section id="specials" class="tab-pane featured-products clearfix m-t-2">
  <div class="products homepage-products-slider">
           
 

  <article class="product-miniature js-product-miniature" data-id-product="2" data-id-product-attribute="9" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/femmes/2-9-brown-bear-printed-sweater.html#/1-taille-s" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/21-home_default/brown-bear-printed-sweater.jpg"
            alt = "Pull imprimé ours brun"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/21-large_default/brown-bear-printed-sweater.jpg"
          >
          
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price">43,08 €</span>
                                  <span class="discount-percentage">-20%</span>
                              
              

              <span itemprop="price" class="price" style="color: ">34,46 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="discount"><span>Prix réduit</span></li>
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/femmes/2-9-brown-bear-printed-sweater.html#/1-taille-s">Pull imprimé colibri</a></h1>
        

        
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price">43,08 €</span>
                                  <span class="discount-percentage">-20%</span>
                              
              

              <span itemprop="price" class="price" style="color: ">34,46 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/femmes/2-9-brown-bear-printed-sweater.html#/1-taille-s" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="1" data-id-product-attribute="1" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/hommes/1-1-hummingbird-printed-t-shirt.html#/1-taille-s/8-couleur-blanc" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/2-home_default/hummingbird-printed-t-shirt.jpg"
            alt = "T-shirt imprimé colibri"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/2-large_default/hummingbird-printed-t-shirt.jpg"
          >
          
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price">28,68 €</span>
                                  <span class="discount-percentage">-20%</span>
                              
              

              <span itemprop="price" class="price" style="color: ">22,94 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="discount"><span>Prix réduit</span></li>
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/hommes/1-1-hummingbird-printed-t-shirt.html#/1-taille-s/8-couleur-blanc">T-shirt imprimé colibri</a></h1>
        

        
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price">28,68 €</span>
                                  <span class="discount-percentage">-20%</span>
                              
              

              <span itemprop="price" class="price" style="color: ">22,94 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/hommes/1-1-hummingbird-printed-t-shirt.html#/1-taille-s/8-couleur-blanc" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
                  <div class="variant-links">
      <a href="http://localhost:8888/prestaRunway/hommes/1-3-hummingbird-printed-t-shirt.html#/2-taille-m/8-couleur-blanc"
       class="color"
       title="Blanc"
              style="background-color: #ffffff"           ><span class="sr-only">Blanc</span></a>
      <a href="http://localhost:8888/prestaRunway/hommes/1-2-hummingbird-printed-t-shirt.html#/1-taille-s/11-couleur-noir"
       class="color"
       title="Noir"
              style="background-color: #434A54"           ><span class="sr-only">Noir</span></a>
    <span class="js-count count"></span>
</div>
              
      
    </div>

  </article>
  


      </div>
</section>
<?php }
}
