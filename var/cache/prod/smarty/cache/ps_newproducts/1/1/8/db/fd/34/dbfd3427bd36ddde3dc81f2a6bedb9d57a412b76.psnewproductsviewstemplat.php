<?php
/* Smarty version 3.1.33, created on 2019-07-10 17:03:25
  from 'module:psnewproductsviewstemplat' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d25fe3d6611f8_95456419',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4a8df44403a47041b050fac755e17268c2a7c3e7' => 
    array (
      0 => 'module:psnewproductsviewstemplat',
      1 => 1554227646,
      2 => 'module',
    ),
    '4c73a61b1c39d845436b133592fdd79d88b50660' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/catalog/_partials/miniatures/product-homepage.tpl',
      1 => 1554227646,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5d25fe3d6611f8_95456419 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
));
?>
<div class="nav-item"><a data-toggle="tab" class="nav-link" href="#new_products">Nouveaux produits</a>
</div>
<section id="new_products" class="tab-pane featured-products clearfix m-t-2">
  <div class="products homepage-products-slider">
           
 

  <article class="product-miniature js-product-miniature" data-id-product="19" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/accessoires-de-maison/19-customizable-mug.html" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/22-home_default/customizable-mug.jpg"
            alt = "Customizable mug"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/22-large_default/customizable-mug.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">16,68 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/accessoires-de-maison/19-customizable-mug.html">Mug personnalisable</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">16,68 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/accessoires-de-maison/19-customizable-mug.html" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="18" data-id-product-attribute="36" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/papeterie/18-36-carnet-de-notes-renard.html#/22-type_de_papier-ligne" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/20-home_default/carnet-de-notes-renard.jpg"
            alt = "Carnet de notes Renard"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/20-large_default/carnet-de-notes-renard.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">15,48 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/papeterie/18-36-carnet-de-notes-renard.html#/22-type_de_papier-ligne">Carnet de notes Colibri</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">15,48 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/papeterie/18-36-carnet-de-notes-renard.html#/22-type_de_papier-ligne" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="17" data-id-product-attribute="32" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/papeterie/17-32-carnet-de-notes-renard.html#/22-type_de_papier-ligne" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/19-home_default/carnet-de-notes-renard.jpg"
            alt = "Carnet de notes Renard"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/19-large_default/carnet-de-notes-renard.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">15,48 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/papeterie/17-32-carnet-de-notes-renard.html#/22-type_de_papier-ligne">Carnet de notes Ours brun</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">15,48 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/papeterie/17-32-carnet-de-notes-renard.html#/22-type_de_papier-ligne" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="16" data-id-product-attribute="28" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/papeterie/16-28-carnet-de-notes-renard.html#/22-type_de_papier-ligne" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/18-home_default/carnet-de-notes-renard.jpg"
            alt = "Carnet de notes Renard"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/18-large_default/carnet-de-notes-renard.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">15,48 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/papeterie/16-28-carnet-de-notes-renard.html#/22-type_de_papier-ligne">Carnet de notes Renard</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">15,48 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/papeterie/16-28-carnet-de-notes-renard.html#/22-type_de_papier-ligne" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="15" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/accessoires-de-maison/15-pack-mug-affiche-encadree.html" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/23-home_default/pack-mug-affiche-encadree.jpg"
            alt = "Pack Mug + Affiche encadrée"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/23-large_default/pack-mug-affiche-encadree.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">42,00 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
                  <li class="pack"><span>Pack</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/accessoires-de-maison/15-pack-mug-affiche-encadree.html">Pack Mug + Affiche encadrée</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">42,00 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/accessoires-de-maison/15-pack-mug-affiche-encadree.html" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="14" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/art/14-illustration-vectorielle-colibri.html" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/17-home_default/illustration-vectorielle-colibri.jpg"
            alt = "Illustration vectorielle Colibri"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/17-large_default/illustration-vectorielle-colibri.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">10,80 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/art/14-illustration-vectorielle-colibri.html">Illustration vectorielle...</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">10,80 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/art/14-illustration-vectorielle-colibri.html" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="13" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/art/13-illustration-vectorielle-ours-brun.html" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/16-home_default/illustration-vectorielle-ours-brun.jpg"
            alt = "Illustration vectorielle Ours brun"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/16-large_default/illustration-vectorielle-ours-brun.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">10,80 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/art/13-illustration-vectorielle-ours-brun.html">Illustration vectorielle...</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">10,80 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/art/13-illustration-vectorielle-ours-brun.html" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


           
 

  <article class="product-miniature js-product-miniature" data-id-product="12" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://localhost:8888/prestaRunway/art/12-illustration-vectorielle-renard.html" class="thumbnail product-thumbnail">
          <img
            src = "http://localhost:8888/prestaRunway/15-home_default/illustration-vectorielle-renard.jpg"
            alt = "Illustration vectorielle Renard"
            data-full-size-image-url = "http://localhost:8888/prestaRunway/15-large_default/illustration-vectorielle-renard.jpg"
          >
          
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">10,80 €</span>

              

            
          </div>
              
      
    
      <ul class="product-flags">
                  <li class="new"><span>Nouveau</span></li>
              </ul>
           
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a style="color: " href="http://localhost:8888/prestaRunway/art/12-illustration-vectorielle-renard.html">Illustration vectorielle...</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span itemprop="price" class="price" style="color: ">10,80 €</span>

              

            
          </div>
              

      
        
      
    </div>

	<div class="more-info-btn-wrap">
	   <a href="http://localhost:8888/prestaRunway/art/12-illustration-vectorielle-renard.html" class="light-button more-info-btn">
		   More Info
	   </a> 
	</div>


    <div class="highlighted-informations no-variants">
      
      <div class="quick-view-wrap hidden-sm-down">
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Aperçu rapide
        </a>
      </div>
      
      
    
	    	      	      <span class='product-availability available'></span>
	    	      

      
              
      
    </div>

  </article>
  


      </div>
</section>


<?php }
}
