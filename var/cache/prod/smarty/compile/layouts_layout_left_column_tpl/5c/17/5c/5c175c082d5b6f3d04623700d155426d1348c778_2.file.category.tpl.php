<?php
/* Smarty version 3.1.33, created on 2019-07-10 17:03:50
  from '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/catalog/listing/category.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d25fe56289fd8_24703763',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5c175c082d5b6f3d04623700d155426d1348c778' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/catalog/listing/category.tpl',
      1 => 1554227646,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/miniatures/category.tpl' => 1,
  ),
),false)) {
function content_5d25fe56289fd8_24703763 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2205353355d25fe56272a15_72789283', 'product_list_header');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'catalog/listing/product-list.tpl');
}
/* {block 'category_miniature'} */
class Block_11304741555d25fe5627d243_01634939 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                  <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/miniatures/category.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('category'=>$_smarty_tpl->tpl_vars['subcategory']->value), 0, true);
?>
                <?php
}
}
/* {/block 'category_miniature'} */
/* {block 'category_subcategories'} */
class Block_19595351845d25fe56279230_80093774 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <aside class="hidden-sm-down clearfix">
      <?php if (count($_smarty_tpl->tpl_vars['subcategories']->value)) {?>
        <nav class="subcategories">
          <ul>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subcategories']->value, 'subcategory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['subcategory']->value) {
?>
              <li>
                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11304741555d25fe5627d243_01634939', 'category_miniature', $this->tplIndex);
?>

              </li>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
          </ul>
        </nav>
      <?php }?>
    </aside>
  <?php
}
}
/* {/block 'category_subcategories'} */
/* {block 'product_list_header'} */
class Block_2205353355d25fe56272a15_72789283 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_header' => 
  array (
    0 => 'Block_2205353355d25fe56272a15_72789283',
  ),
  'category_subcategories' => 
  array (
    0 => 'Block_19595351845d25fe56279230_80093774',
  ),
  'category_miniature' => 
  array (
    0 => 'Block_11304741555d25fe5627d243_01634939',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <div class="block-category row hidden-sm-down">
  <div id="category-description" class="col-md-7">
	  <h1><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8');?>
</h1>
	  <?php echo $_smarty_tpl->tpl_vars['category']->value['description'];?>
</div>    
  <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['image']['large']['url'], ENT_QUOTES, 'UTF-8');?>
" class="col-md-4" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['image']['legend'], ENT_QUOTES, 'UTF-8');?>
">	  
  </div>
  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19595351845d25fe56279230_80093774', 'category_subcategories', $this->tplIndex);
?>

 

<?php
}
}
/* {/block 'product_list_header'} */
}
