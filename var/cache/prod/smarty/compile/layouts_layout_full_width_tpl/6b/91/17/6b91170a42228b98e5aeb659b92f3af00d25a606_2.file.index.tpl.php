<?php
/* Smarty version 3.1.33, created on 2019-07-10 17:03:19
  from '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d25fe37e1b524_36970592',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6b91170a42228b98e5aeb659b92f3af00d25a606' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/index.tpl',
      1 => 1554227646,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d25fe37e1b524_36970592 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2503603335d25fe37e0fdd4_94828166', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_2952122175d25fe37e11475_84521671 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_20726161095d25fe37e14bb8_33864096 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            
        <div class="clearfix"></div>
    	<div class="tab-content" id="homepage-tabs">
        	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayHomeTabs'),$_smarty_tpl ) );?>

    	</div>
    	<div class="clearfix"></div>              
          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_8379610865d25fe37e134c0_43139240 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20726161095d25fe37e14bb8_33864096', 'hook_home', $this->tplIndex);
?>

          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayHomeFooter'),$_smarty_tpl ) );?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_2503603335d25fe37e0fdd4_94828166 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_2503603335d25fe37e0fdd4_94828166',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_2952122175d25fe37e11475_84521671',
  ),
  'page_content' => 
  array (
    0 => 'Block_8379610865d25fe37e134c0_43139240',
  ),
  'hook_home' => 
  array (
    0 => 'Block_20726161095d25fe37e14bb8_33864096',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2952122175d25fe37e11475_84521671', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8379610865d25fe37e134c0_43139240', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
