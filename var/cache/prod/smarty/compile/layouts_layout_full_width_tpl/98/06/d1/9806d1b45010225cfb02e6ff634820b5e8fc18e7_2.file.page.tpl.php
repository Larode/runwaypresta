<?php
/* Smarty version 3.1.33, created on 2019-07-10 17:03:19
  from '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d25fe37e31e03_35938809',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9806d1b45010225cfb02e6ff634820b5e8fc18e7' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/page.tpl',
      1 => 1554227646,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d25fe37e31e03_35938809 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16101856245d25fe37e26248_05490988', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_995070125d25fe37e28048_58473119 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_20723727615d25fe37e26fc6_08099775 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_995070125d25fe37e28048_58473119', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_4671675955d25fe37e2c265_94533087 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_3841948435d25fe37e2d454_47582393 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_9421527845d25fe37e2b5a6_33896772 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4671675955d25fe37e2c265_94533087', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3841948435d25fe37e2d454_47582393', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_11538326305d25fe37e2fc20_94949240 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_331709985d25fe37e2eec4_09449877 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11538326305d25fe37e2fc20_94949240', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_16101856245d25fe37e26248_05490988 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_16101856245d25fe37e26248_05490988',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_20723727615d25fe37e26fc6_08099775',
  ),
  'page_title' => 
  array (
    0 => 'Block_995070125d25fe37e28048_58473119',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_9421527845d25fe37e2b5a6_33896772',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_4671675955d25fe37e2c265_94533087',
  ),
  'page_content' => 
  array (
    0 => 'Block_3841948435d25fe37e2d454_47582393',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_331709985d25fe37e2eec4_09449877',
  ),
  'page_footer' => 
  array (
    0 => 'Block_11538326305d25fe37e2fc20_94949240',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20723727615d25fe37e26fc6_08099775', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9421527845d25fe37e2b5a6_33896772', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_331709985d25fe37e2eec4_09449877', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
