<?php
/* Smarty version 3.1.33, created on 2019-07-10 17:03:26
  from '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/_partials/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d25fe3ed61704_17676816',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4a02e7f7c9a551f7ecf40adaa90be1003d6c8178' => 
    array (
      0 => '/Applications/MAMP/htdocs/prestaRunway/themes/runway/templates/_partials/footer.tpl',
      1 => 1554227646,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d25fe3ed61704_17676816 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<div class="container">
  <div class="row">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13434668165d25fe3ed51129_28305615', 'hook_footer_before');
?>

  </div>
</div>
 <?php if (Module::isEnabled('belvg_themeconfigurator')) {?>
	<?php $_smarty_tpl->_assignInScope('belvg_color_left_footer_bg', Configuration::get('COLOR_VAL_LEFT_FOOTER_BG'));?>	
<?php }?> 
<div class="footer-container" style="background: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['belvg_color_left_footer_bg']->value, ENT_QUOTES, 'UTF-8');?>
">
  <div class="container-fluid">
    <div class="row">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1971767125d25fe3ed57203_82501971', 'hook_footer');
?>

	  <div class="contact-info-wrap col-md-5 col-sm-12">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17807144165d25fe3ed59c82_89142028', 'hook_footer_after');
?>

	  </div>
    </div>
    <div class="row copyright">
      <div class="col-md-12">
          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8106298225d25fe3ed5c273_27845818', 'copyright_link');
?>

      </div>
    </div>
  </div>
</div>
<a id="scroll-top" href="#header">
	<svg version="1.1" id="Capa_1" width="15px" height="15px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="451.847px" height="451.846px" viewBox="0 0 451.847 451.846" style="enable-background:new 0 0 451.847 451.846;"
	 xml:space="preserve">
<g>
	<path d="M248.292,106.406l194.281,194.29c12.365,12.359,12.365,32.391,0,44.744c-12.354,12.354-32.391,12.354-44.744,0
		L225.923,173.529L54.018,345.44c-12.36,12.354-32.395,12.354-44.748,0c-12.359-12.354-12.359-32.391,0-44.75L203.554,106.4
		c6.18-6.174,14.271-9.259,22.369-9.259C234.018,97.141,242.115,100.232,248.292,106.406z"/>
</g>
</svg>
</a>
<?php }
/* {block 'hook_footer_before'} */
class Block_13434668165d25fe3ed51129_28305615 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_before' => 
  array (
    0 => 'Block_13434668165d25fe3ed51129_28305615',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterBefore'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_footer_before'} */
/* {block 'hook_footer'} */
class Block_1971767125d25fe3ed57203_82501971 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer' => 
  array (
    0 => 'Block_1971767125d25fe3ed57203_82501971',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooter'),$_smarty_tpl ) );?>

      <?php
}
}
/* {/block 'hook_footer'} */
/* {block 'hook_footer_after'} */
class Block_17807144165d25fe3ed59c82_89142028 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_after' => 
  array (
    0 => 'Block_17807144165d25fe3ed59c82_89142028',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterAfter'),$_smarty_tpl ) );?>

      <?php
}
}
/* {/block 'hook_footer_after'} */
/* {block 'copyright_link'} */
class Block_8106298225d25fe3ed5c273_27845818 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'copyright_link' => 
  array (
    0 => 'Block_8106298225d25fe3ed5c273_27845818',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <a class="_blank" href="http://www.prestashop.com" target="_blank">
              <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'%copyright% %year% - Ecommerce software by %prestashop%','sprintf'=>array('%prestashop%'=>'PrestaShop™','%year%'=>date('Y'),'%copyright%'=>'©'),'d'=>'Shop.Theme'),$_smarty_tpl ) );?>

            </a>
          <?php
}
}
/* {/block 'copyright_link'} */
}
