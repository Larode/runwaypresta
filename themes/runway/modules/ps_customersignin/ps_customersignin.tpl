{if Module::isEnabled('belvg_themeconfigurator')}
	{assign var = belvg_color_value_left_icons value = Configuration::get('COLOR_VAL_LEFT_ICONS')} 
{/if}
<div id="_desktop_user_info">
  <div class="user-info">
    {if $logged}
    <div id="user-toggle">
	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 563.43 563.43" style="enable-background:new 0 0 563.43 563.43; fill:{$belvg_color_value_left_icons}" xml:space="preserve" width="19px" height="19px">
<path d="M280.79,314.559c83.266,0,150.803-67.538,150.803-150.803S364.055,13.415,280.79,13.415S129.987,80.953,129.987,163.756  S197.524,314.559,280.79,314.559z M280.79,52.735c61.061,0,111.021,49.959,111.021,111.021S341.851,274.776,280.79,274.776  s-111.021-49.959-111.021-111.021S219.728,52.735,280.79,52.735z"></path>
<path d="M19.891,550.015h523.648c11.102,0,19.891-8.789,19.891-19.891c0-104.082-84.653-189.198-189.198-189.198H189.198  C85.116,340.926,0,425.579,0,530.124C0,541.226,8.789,550.015,19.891,550.015z M189.198,380.708h185.034  c75.864,0,138.313,56.436,148.028,129.524H41.17C50.884,437.607,113.334,380.708,189.198,380.708z"></path>
<g>
</g>
</svg>
</div>
<div class="logged-wrapper">
      <a
        class="logout"
        href="{$logout_url}"
        rel="nofollow"
      >
{*         <i class="material-icons">&#xE7FF;</i> *}
        {l s='Sign out' d='Shop.Theme.Actions'}
      </a>
      <a
        class="account"
        href="{$my_account_url}"
        title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}"
        rel="nofollow"
      >
{*         <i class="material-icons hidden-md-up logged">&#xE7FF;</i> *}
        <span class="hidden-sm-down">{$customerName}</span>
        <span class="hidden-sm-up">{l s='Account' d='Shop.Theme.Customeraccount'}</span>
      </a>
</div>
    {else}
      <a
        href="{$my_account_url}"
        title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}"
        rel="nofollow"
      >
        		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 563.43 563.43" style="enable-background:new 0 0 563.43 563.43; fill: {$belvg_color_value_left_icons}" xml:space="preserve" width="19px" height="19px">
<path d="M280.79,314.559c83.266,0,150.803-67.538,150.803-150.803S364.055,13.415,280.79,13.415S129.987,80.953,129.987,163.756  S197.524,314.559,280.79,314.559z M280.79,52.735c61.061,0,111.021,49.959,111.021,111.021S341.851,274.776,280.79,274.776  s-111.021-49.959-111.021-111.021S219.728,52.735,280.79,52.735z"></path>
<path d="M19.891,550.015h523.648c11.102,0,19.891-8.789,19.891-19.891c0-104.082-84.653-189.198-189.198-189.198H189.198  C85.116,340.926,0,425.579,0,530.124C0,541.226,8.789,550.015,19.891,550.015z M189.198,380.708h185.034  c75.864,0,138.313,56.436,148.028,129.524H41.17C50.884,437.607,113.334,380.708,189.198,380.708z"></path>
<g>
</g>
</svg>
{*         <span class="hidden-sm-down">{l s='Sign in' d='Shop.Theme.Actions'}</span> *}
      </a>
    {/if}
  </div>
</div>
