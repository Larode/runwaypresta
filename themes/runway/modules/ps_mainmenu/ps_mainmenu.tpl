{if Module::isEnabled('belvg_themeconfigurator')}
	{assign var = belvg_color_value_left_nav_bg value = Configuration::get('COLOR_VAL_LEFT_NAV_BG')} 
	{assign var = belvg_color_value_left_menu value = Configuration::get('COLOR_VAL_LEFT_MENU')} 
{/if}
{assign var=_counter value=0}
{function name="menu" nodes=[] depth=0 parent=null}
    {if $nodes|count}
      <ul class="top-menu" {if $depth == 0}id="top-menu"{/if} data-depth="{$depth}">
        {foreach from=$nodes item=node}
            <li class="{$node.type}{if $node.current} current {/if}" id="{$node.page_identifier}">
            {assign var=_counter value=$_counter+1}
              <a
                class="{if $depth >= 0}dropdown-item{/if}{if $depth === 1} dropdown-submenu{/if}" {if $depth !== 1 && $depth !== 2 }style="color: {$belvg_color_value_left_menu}"{/if}
                href="{$node.url}" data-depth="{$depth}"
                {if $node.open_in_new_window} target="_blank" {/if}
              >
	              
{*
	              {foreach from=$urls.img_cat_url item=img_cat_url}
	              	{if $depth >= 0}<img src="{$img_cat_url}3-category_default.jpg" width="30px">{/if}
	              {/foreach}
*}
{*
                  {foreach from=$node item=node}
                    <img src="{$node.image_url}" width="30px">  
                  {/foreach}
*}
	              
                {if $node.children|count}
                  {* Cannot use page identifier as we can have the same page several times *}
                  {assign var=_expand_id value=10|mt_rand:100000}
                  <span class="pull-xs-right hidden-md-up">
                    <span data-target="#top_sub_menu_{$_expand_id}" data-toggle="collapse" class="navbar-toggler collapse-icons">
                      <i class="material-icons add">&#xE313;</i>
                      <i class="material-icons remove">&#xE316;</i>
                    </span>
                  </span>
                {/if}
                {$node.label}
              </a>
              {if $node.children|count}
              <div {if $depth === 0} class="popover sub-menu js-sub-menu collapse"{else} class="collapse"{/if} id="top_sub_menu_{$_expand_id}">
                {menu nodes=$node.children depth=$node.depth parent=$node}
                <div class="menu-images-container">
                  {foreach from=$node.image_urls item=image_url}
                    <a href={$node.url}><img src="{$image_url}"></a>
                  {/foreach}
{*                   <a href={$node.url}>{l s='Shop' d='Shop.Theme.Actions'} {$node.label} </a> *}
                </div>                
              </div>
              {/if}
            </li>
        {/foreach}
      </ul>
    {/if}
{/function}

<div class="menu col-lg-12 col-md-12 js-top-menu position-static hidden-sm-down" id="_desktop_top_menu" style="background:{$belvg_color_value_left_nav_bg}">
    {menu nodes=$menu.children}
    <div class="clearfix"></div>
</div>
