/**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
import $ from 'jquery';

$(document).ready(function () {

var tabs = $('.tab-content .nav-item'); 
	//declare first tab as active
	tabs.eq(0).find('a').addClass('active');
	tabs.eq(0).next().addClass('active');	
 	
		tabs.click(function(){
			//add active class to tab that was clicked
			$(this).find('a').addClass('active');
			$(this).next().addClass('active');
			
			//remove active class from siblings tabs
			$(this).siblings().find('a').removeClass('active');
			$(this).siblings().next().removeClass('active');
		});	

var fixedMenu = $('#header .header-top .position-static');
var startPos = 	fixedMenu.offset().top + 20;
	$(window).scroll(function(e){		
		$(window).scrollTop()>startPos ? fixedMenu.addClass('fixed-on-scroll') : fixedMenu.removeClass('fixed-on-scroll'); 	       
		function moveDown(){
			$('.fixed-on-scroll').css('top','-15px');  
		}
	});

	 
$('.search-toggle').click(function(){
	$('#search_widget form').toggleClass('show');
});

$('#lang-toggle').click(function(){
	$('.lang_wrapper__inner').toggleClass('show');
});	

$('#user-toggle').click(function(){
	$('.logged-wrapper').toggleClass('show');
});	

//Product Hover
/*
$('.thumbnail-container').each(function(i){
	$(this).find($('.product-description')).on('mouseover', function(){
		$(this).find($('.variant-links')).css('opacity', 1); 
	});
	$(this).find($('.product-description')).on('mouseout', function(){
		$(this).find($('.variant-links')).css('opactity', 0); 
	});	
});
*/
/*
$('.thumbnail-container .product-description').on('mouseover', function(){
	$('.variant-links').css('opacity', 1);
});

$('.product-description').on('mouseout', function(){
	$('.variant-links').css('opactity', 0);
});
*/

//Dropdown Menu Width
var RunwayMenu = function(){
   var menuItem = $('#_desktop_top_menu>.top-menu>li');
   
	  menuItem.on('mouseover', function(){
		  var menuItemOffset = $(this).offset();

		  var cat = $(this).find('.popover>.top-menu>li');
	
		  var catDropdownWdth = cat.length * 150;	  	  

		$(this).find('.popover').show();
		 var menuWidth = $(this).find('.popover').width();
		
		   
		   var widthLimit = $(window).width() - menuItemOffset.left;
		   if ( catDropdownWdth >= widthLimit ) {   
			   $(this).find('.popover').width(widthLimit);  
		   } 
		   else {
			    $('.menu .popover').width(catDropdownWdth);
		   }		   
	
		   if ( menuItemOffset.left > $(window).width()/2 ) {
			   $(this).find('.popover').css({
				   'left': 'inherit',
				   'right': 0
			   });				   
		
			   if ( catDropdownWdth >= menuItemOffset.left ) {   
				   catDropdownWdth = menuItemOffset.left;
				   $(this).find('.popover').width(catDropdownWdth);
				      
			   } 
			   else {
				    $(this).find('.popover').width(catDropdownWdth);
			   }			     
		   }
		   
	  });
} 
	  
	function resize(){
		RunwayMenu();
	}
	resize();
	$(window).resize(resize);  	  
	  
	 
});
 

