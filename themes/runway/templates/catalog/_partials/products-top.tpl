{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id="js-product-list-top" class="row products-selection">
  <div class="hidden-sm-down total-products">
    {if $listing.pagination.total_items > 1}
      <p>{l s='There are %product_count% products.' d='Shop.Theme.Catalog' sprintf=['%product_count%' => $listing.pagination.total_items]}</p>
    {else if $listing.pagination.total_items > 0}
      <p>{l s='There is 1 product.' d='Shop.Theme.Catalog'}</p>
    {/if}
  </div>
  <div class="sort-by-wrapper">
    <div class="row sort-by-row">

      {block name='sort_by'}
        {include file='catalog/_partials/sort-orders.tpl' sort_orders=$listing.sort_orders}
      {/block}

      {if !empty($listing.rendered_facets)}
        <div class=" hidden-md-up filter-button">
          <button id="search_filter_toggler" class="btn btn-primary">
            {l s='Filter' d='Shop.Theme.Actions'}
          </button>
        </div>
      {/if}
    </div>
  </div>
{*   {hook h='displayFilters'} *}
       <div class="belvg_themeconfigurator__grid-switch hidden-sm-down">
{* 	        <div style="display: inline-block; margin-right: 5px; font-weight: bold; color:black;">Grid:</div> *}
	      <span data-grid="2"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="18px" height="18px" viewBox="0 0 401.991 401.991" style="enable-background:new 0 0 401.991 401.991; margin-left: 5px;" xml:space="preserve">
<g>
	<g>
		<path d="M228.405,146.179h-54.816c-7.613,0-14.084,2.665-19.414,7.995c-5.33,5.33-7.994,11.798-7.994,19.414v54.82    c0,7.617,2.665,14.086,7.994,19.41c5.33,5.332,11.801,7.994,19.414,7.994h54.816c7.614,0,14.083-2.662,19.411-7.994    c5.328-5.324,7.994-11.793,7.994-19.41v-54.82c0-7.616-2.666-14.087-7.994-19.414    C242.488,148.844,236.019,146.179,228.405,146.179z"></path>
		<path d="M82.224,146.179H27.406c-7.611,0-14.084,2.665-19.414,7.995C2.662,159.503,0,165.972,0,173.587v54.82    c0,7.617,2.662,14.086,7.992,19.41c5.33,5.332,11.803,7.994,19.414,7.994h54.818c7.611,0,14.084-2.662,19.414-7.994    c5.33-5.324,7.994-11.793,7.994-19.41v-54.82c0-7.616-2.664-14.087-7.994-19.414C96.308,148.847,89.835,146.179,82.224,146.179z"></path>
	</g>
</g>
</svg></span>
	      <span data-grid="3" class="current"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="18px" height="18px" viewBox="0 0 401.991 401.991" style="enable-background:new 0 0 401.991 401.991;" xml:space="preserve">
<g>
	<g>
		<path d="M228.405,146.179h-54.816c-7.613,0-14.084,2.665-19.414,7.995c-5.33,5.33-7.994,11.798-7.994,19.414v54.82    c0,7.617,2.665,14.086,7.994,19.41c5.33,5.332,11.801,7.994,19.414,7.994h54.816c7.614,0,14.083-2.662,19.411-7.994    c5.328-5.324,7.994-11.793,7.994-19.41v-54.82c0-7.616-2.666-14.087-7.994-19.414    C242.488,148.844,236.019,146.179,228.405,146.179z"></path>
		<path d="M82.224,146.179H27.406c-7.611,0-14.084,2.665-19.414,7.995C2.662,159.503,0,165.972,0,173.587v54.82    c0,7.617,2.662,14.086,7.992,19.41c5.33,5.332,11.803,7.994,19.414,7.994h54.818c7.611,0,14.084-2.662,19.414-7.994    c5.33-5.324,7.994-11.793,7.994-19.41v-54.82c0-7.616-2.664-14.087-7.994-19.414C96.308,148.847,89.835,146.179,82.224,146.179z"></path>
		<path d="M394,154.174c-5.331-5.33-11.806-7.995-19.417-7.995h-54.819c-7.621,0-14.089,2.665-19.418,7.995    c-5.328,5.33-7.994,11.798-7.994,19.414v54.82c0,7.617,2.666,14.086,7.994,19.41c5.329,5.332,11.797,7.994,19.418,7.994h54.819    c7.611,0,14.086-2.662,19.417-7.994c5.325-5.324,7.991-11.793,7.991-19.41v-54.82C401.991,165.972,399.332,159.5,394,154.174z"></path>
	</g>
</g>
</svg></span>
	      <span data-grid="4"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 381.39 381.39" width="13px" height="13px" style="enable-background:new 0 0 381.39 381.39;" xml:space="preserve">
<g>
	<g>
		<g>
			<path d="M127.13,0H31.782C14.239,0,0,14.239,0,31.782v95.347c0,17.544,14.239,31.782,31.782,31.782h95.347     c17.544,0,31.782-14.239,31.782-31.782V31.782C158.912,14.239,144.674,0,127.13,0z"></path>
			<path d="M349.607,0H254.26c-17.544,0-31.782,14.239-31.782,31.782v95.347     c0,17.544,14.239,31.782,31.782,31.782h95.347c17.544,0,31.782-14.239,31.782-31.782V31.782C381.39,14.239,367.151,0,349.607,0z"></path>
			<path d="M127.13,222.477H31.782C14.239,222.477,0,236.716,0,254.26v95.347     c0,17.544,14.239,31.782,31.782,31.782h95.347c17.544,0,31.782-14.239,31.782-31.782V254.26     C158.912,236.716,144.674,222.477,127.13,222.477z"></path>
			<path d="M349.607,222.477H254.26c-17.544,0-31.782,14.239-31.782,31.782v95.347     c0,17.544,14.239,31.782,31.782,31.782h95.347c17.544,0,31.782-14.239,31.782-31.782V254.26     C381.39,236.716,367.151,222.477,349.607,222.477z"></path>
		</g>
	</g>
</g>
</svg></span>
	      <span data-grid="5"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="18px" height="18px" viewBox="0 0 965.199 965.199" style="enable-background:new 0 0 965.199 965.199;" xml:space="preserve">
<g>
	<path d="M263.85,30c0-16.6-13.4-30-30-30h-202c-16.6,0-30,13.4-30,30v202.1c0,16.6,13.4,30,30,30h202.1c16.6,0,30-13.4,30-30V30   H263.85z"></path>
	<path d="M613.55,30c0-16.6-13.4-30-30-30h-202c-16.6,0-30,13.4-30,30v202.1c0,16.6,13.4,30,30,30h202c16.6,0,30-13.4,30-30V30z"></path>
	<path d="M963.25,30c0-16.6-13.4-30-30-30h-202c-16.601,0-30,13.4-30,30v202.1c0,16.6,13.399,30,30,30h202.1c16.601,0,30-13.4,30-30   V30H963.25z"></path>
	<path d="M263.85,381.6c0-16.6-13.4-30-30-30h-202c-16.6,0-30,13.4-30,30v202c0,16.6,13.4,30,30,30h202.1c16.6,0,30-13.4,30-30v-202   H263.85z"></path>
	<path d="M613.55,381.6c0-16.6-13.4-30-30-30h-202c-16.6,0-30,13.4-30,30v202c0,16.6,13.4,30,30,30h202c16.6,0,30-13.4,30-30V381.6z   "></path>
	<path d="M963.25,381.6c0-16.6-13.4-30-30-30h-202c-16.601,0-30,13.4-30,30v202c0,16.6,13.399,30,30,30h202.1   c16.601,0,30-13.4,30-30v-202H963.25z"></path>
	<path d="M233.85,703.1h-202c-16.6,0-30,13.4-30,30v202.1c0,16.602,13.4,30,30,30h202.1c16.6,0,30-13.398,30-30V733.1   C263.85,716.6,250.45,703.1,233.85,703.1z"></path>
	<path d="M583.55,703.1h-202c-16.6,0-30,13.4-30,30v202.1c0,16.602,13.4,30,30,30h202c16.6,0,30-13.398,30-30V733.1   C613.55,716.6,600.149,703.1,583.55,703.1z"></path>
	<path d="M933.25,703.1h-202c-16.601,0-30,13.4-30,30v202.1c0,16.602,13.399,30,30,30h202.1c16.601,0,30-13.398,30-30V733.1   C963.25,716.6,949.85,703.1,933.25,703.1z"></path>
</g>
</svg></span>      
      </div>
  <div class="col-sm-12 hidden-md-up text-xs-center showing">
    {l s='Showing %from%-%to% of %total% item(s)' d='Shop.Theme.Catalog' sprintf=[
    '%from%' => $listing.pagination.items_shown_from ,
    '%to%' => $listing.pagination.items_shown_to,
    '%total%' => $listing.pagination.total_items
    ]}
  </div>
</div>
