{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{*
 {if Module::isEnabled('belvg_themeconfigurator')}
	{assign var = belvg_color_value_tabs_color value = Configuration::get('COLOR_VAL_TABS_COLOR')}
	{assign var = belvg_color_value_tabs_active_color value = Configuration::get('COLOR_VAL_TABS_ACTIVE_COLOR')}	
	{assign var = live_demo value=Configuration::get('LIVE_DEMO')}   
	{if $live_demo == 0}
    	<style type="text/css">
	    	#home-page-tabs .nav-link.active{
		    	color: {$belvg_color_value_tabs_active_color} !important;
		    	border-color: {$belvg_color_value_tabs_active_color} !important;
		    }
	    </style>
	{/if}	
{/if}  
*}

    {block name='page_content_container'}
      <section id="content" class="page-home">
        {block name='page_content_top'}{/block}

        {block name='page_content'}
          {block name='hook_home'}
            
        <div class="clearfix"></div>
    	<div class="tab-content" id="homepage-tabs">
{*
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
*}
        	{hook h='displayHomeTabs'}
    	</div>
    	<div class="clearfix"></div>              
          {/block}
          {hook h='displayHomeFooter'}
        {/block}
      </section>
    {/block}
